a = True
b = False
c = True

print(a and a)
print(a and b)
print(not c)
print(not b)
print(not a)
print(a and b)
print(a and c)
print(a or c)
print(b or c)
print(c or a)
print(c or b)
print(c or c)
print(b or b)
