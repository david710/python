'''
str - String
'''

print('Olá')
print('Alguma coisa')
print("Aspas duplas")
print(123456)
print('Essa é uma string (str)')
print("Essa é outra 'string' (str)")
print("Esse é meu \"texto\" (str)") #caractere de escape
print(r'Esse é meu \n (str)') # Usando r antes do texto.

