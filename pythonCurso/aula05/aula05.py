'''
Operaqdores aritméticos:

+ : soma
- : subtração
* : multiplicação
/ : divisão
// : divisão inteira
** : potenciação
% : resto da divisão
() : parẽntesis
'''

print('Multiplicação *', 10 * 10)
print('Adição +', 10 + 10)
print('Subtração -',10 - 5)
print('Divisão /', 10 / 2 )
print(10,5 // 3)
print(10**2)
print(10 % 3)
print((10+2)*2)
print(20 * 'A')
print(20 * 2)
print('20' + 'A')
print(20 + 2)
