''''
Tipos de dados

str - String
int - Inteiro
float - real/ponto flutuante
bool - booleano/logico

'''

print('David', type('David'))
print(10, type(10))
print(25.23, type(25.23))
print(10 == 10, type(10 == 10))
print('David', type('David'), bool('David'))
print('10', type('10'),type(int('10')))
print(10, type(10),type(float(10)))
print(10 + 10)
print('10' + '10')

#Exercicio
print('David', type('David'))
print(36, type(36))
print(1.67, type(1.67))
print(36 >= 18, type(36 >= 18))
